import express from "express";
import { v4 as uuidv4 } from "uuid";

const app = express();
const port = 3000

app.use(express.json());

let USERS = [];

const verifyCpf = (request, response, next) => {
  const user = USERS.find((item) => item.cpf === request.params.cpf);
  if (user === undefined) {
    return response
      .status(404)
      .json({ error: "invalid cpf - user is not registered" });
  }
  return next();
};

/**
 * ROTA USERS
 */

// GET
app.get("/users", (req, res) => {
  res.json(USERS);
});

// POST
app.post("/users", (req, res) => {
  const { body } = req;
  const newUser = {
    id: uuidv4(),
    name: `${body.name}`,
    cpf: `${body.cpf}`,
    notes: [],
  };
  const finder = USERS.find((item) => item.cpf === body.cpf);
  if (finder === undefined) {
    USERS.push(newUser);
    return res.status(201).json(newUser);
  }
  res.status(400).json({ message: "cpf already exists" });
});

// PATCH
app.patch("/users/:cpf", verifyCpf, (req, res) => {
  const { cpf } = req.params;
  const { name, cpf: updatedCpf } = req.body;
  const user = USERS.find((item) => item.cpf === cpf);

  if (name) {
    user.name = name;
  }
  if (updatedCpf) {
    user.cpf = updatedCpf;
  }

  res.status(200).json(user);
});

// DELETE
app.delete("/users/:cpf", verifyCpf, (req, res) => {
  const { cpf } = req.params;

  const filtered = USERS.filter((item) => item.cpf !== cpf);

  USERS = filtered;
  res.status(204).json([]);
});

/**
 * ROTA USERS
 */

/**
 * ROTA NOTES
 */

// GET
app.get("/users/:cpf/notes", verifyCpf, (req, res) => {
  const params = req.params;
  const user = USERS.find((item) => item.cpf === params.cpf);

  if (user.notes.length === undefined) {
    return res.status(200).json([]);
  }
  res.status(200).json(user.notes);
});

// POST
app.post("/users/:cpf/notes", verifyCpf, (req, res) => {
  const params = req.params;
  const user = USERS.find((item) => item.cpf === params.cpf);
  const body = req.body;
  const date = new Date();

  const newNote = {
    id: uuidv4(),
    title: body.title,
    content: body.content,
    created_at: date.toLocaleString(),
  };

  const finder = user.notes.find((item) => item.title === body.title);
  if (finder === undefined) {
    user.notes.push(newNote);

    return res.status(201).json({
      message: `${newNote.title} was added into ${user.name}'s notes`,
    });
  }
  res.status(400).json({ message: "this title already existis" });
});

// PATCH
app.patch("/users/:cpf/notes/:id", verifyCpf, (req, res) => {
  const { cpf, id } = req.params;
  const { title, content } = req.body;
  const user = USERS.find((item) => item.cpf === cpf);

  if (title !== undefined) {
    const note = user.notes.find((item) => item.id === id);
    if (note) {
      note.title = title;
    } else {
      return res.status(400).json({messa: "note not found"})
    }
  }
  if (content !== undefined) {
    const note = user.notes.find((item) => item.id === id);
    if (note) {
      note.content = content;
    } else {
      return res.status(400).json({messa: "note not found"})
    }
  }

  return res.status(200).json(user);
});

// DELETE
app.delete("/users/:cpf/notes/:id", verifyCpf, (req, res) => {
  const { cpf, id } = req.params;

  const user = USERS.find((item) => item.cpf === cpf);
  const noteFind = user.notes.find((item) => item.id === id);

  if (noteFind !== undefined) {
    const filtered = user.notes.filter((item) => item.id !== id);
    user.notes = filtered;
    return res.status(204).json([]);
  }
  res.status(400).json({ message: "note not found" });
});
/**
 * ROTA NOTES
 */

/**
 * LISTEN
 */

app.listen(port, () => {
  console.log(`Runnig at http://localhost:${port}`);
});

/**
 * LISTEN
 */
