# Aplicação de Armazenamento de notas

## Descrição
API desenvolvida a fim de adiquirir conhecimentos em node.js, onde é permitido criar um usuário e notas referentes aos mesmos.

## Instalação
Ao clonar o repositório rode o seguinte comando no terminal:

```cmd
yarn
```

ou

```cmd
npm install
```

## Utilização
Em um client de consumo de API (insomnia, postman, etc), faça requisições na **url base**: http://localhost:3000/

## Rotas:

### Rota Users: 

**GET**: **url_base**/users

Não é necessário corpo da requisição

**POST**: **url_base**/users

Corpo da requisição:

```json
{
	"name": "rafael",
	"cpf": "12345678900"
}
```

Se a requisição obtiver sucesso a seguinte mensagem retornará:

```json
{
	"id": "c5fc1ceb-6d19-414c-bcb4-f6d68513814f",
	"name": "rafael",
	"cpf": "12345678900",
	"notes": []
}
```

Caso tente enviar um CPF já cadastrado, a seguinte mensagem será exibida com o código ```400 Bad Request ```:

```json
{
	"message": "cpf already exists"
}
```

Qualquer chave e valor passada além de 'cpf' e 'name' será desconsiderada

**PATCH**: **url_base**/users/*cpf*

Corpo da requisição:

```json
{
	"name": "Rafael Jagochitz Bertoldo"
}
```

Se a requisição obtiver sucesso a seguinte mensagem retornará:

```json
{
	"id": "c5fc1ceb-6d19-414c-bcb4-f6d68513814f",
	"name": "Rafael Jagochitz Bertoldo",
	"cpf": "12345678900",
	"notes": []
}
```

Caso tente editar um usuário inexistente, a seguinte mensagem será exibida com o código ```404 Not Found ```:

```json
{
	"error": "invalid cpf - user is not registered"
}
```

Qualquer chave e valor passada além de 'name' será desconsiderada

**DELETE**: **url_base**/users/*cpf*

Não é necessário corpo da requisição

Caso a requsição obtenha sucesso retornará o código ```204 No Content```

Caso tente deletar um usuário inexistente, a seguinte mensagem será exibida com o código ```404 Not Found ```:

```json
{
	"error": "invalid cpf - user is not registered"
}
```

### Rota Notes: 

**GET**: **url_base**/users/*cpf*/notes

Não é necessário corpo da requisição

Caso o usuário não possua nenhuma nota o retorno será: ```[]```, com o código ```200 OK``` 

Caso tente listar as notas de um usuário inexistente, a seguinte mensagem será exibida com o código ```404 Not Found ```:

```json
{
	"error": "invalid cpf - user is not registered"
}
```

**POST**: **url_base**/users`/*cpf*/notes

Corpo da requisição:

```json
{
	"title": "Assistir One Piece",
	"content": "Colocar em dia o episódio semanal"
}
```

Se a requisição obtiver sucesso a seguinte mensagem retornará:

```json
{
	"message": "Assistir One Piece was added into rafael's notes"
}
```
Com o código ```201 Created```

Caso tente criar uma nota para um CPF inexistente, a seguinte mensagem será exibida com o código ```400 Bad Request ```:

```json
{
	"error": "invalid cpf - user is not registered"
}
```

Qualquer chave e valor passada além de ```title``` e ```content``` será desconsiderada

**PATCH**: **url_base**/users/*cpf*/notes

Corpo da requisição:

```json
{
	"title": "Assistir One Piece X",
	"content": "Infelizmente hoje não será possível, pretendo assistir amanhã"
}
```

Se a requisição obtiver sucesso a seguinte mensagem retornará:

```json
{
	"id": "a6df894b-3469-4666-a828-c33ffe8a4ae2",
	"name": "rafael",
	"cpf": "12345678901",
	"notes": [
		{
			"id": "97d87022-49a6-4fee-8f22-81b5165a7d1b",
			"title": "Assistir One Piece X",
			"content": "Infelizmente hoje não será possível, pretendo assistir amanhã",
			"created_at": "11/01/2022 00:15:01"
		}
	]
}
```

Qualquer chave e valor passada além de ```title``` e ```content``` será desconsiderada

**DELETE**: **url_base**/users/*cpf*/notes/*title*

Não é necessário corpo da requisição

Caso a requsição obtenha sucesso retornará o código ```204 No Content```

Caso tente deletar uma nota inexistente, a seguinte mensagem será exibida com o código ```404 Not Found ```:

```json
{
	"message": "note not found"
}
```